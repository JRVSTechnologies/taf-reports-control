<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of projectcontrol
 *
 * @author jossie
 */
class projectcontrol extends reportsuite {

    public static $pathToFramework = 'sites/all/modules/automation/TestFramework/';
    public static $jenkinsToken = 'STAF3F8D7D8G4FRFR3E4R';
    public static $JIRAPlaceholder = 'JIRA: TDP-62957 ';
    //Automation Suite Portal
    public static $ASPproductionIP = '10.229.4.95';

    public static function sortProjectListByCustomerAlias($listOfProject) {
        $listOfProjectOverride = array();
        $listOfProjectAdjustment = array();
        foreach ($listOfProject as $tpid => $projectData) {
            $listOfProjectOverride[$projectData['CustomerAlias']] =
                    $projectData;
        }
        ksort($listOfProjectOverride);
        foreach ($listOfProjectOverride as $customerAlias => $projectData) {
            $listOfProjectAdjustment[$projectData['tpid']] = $projectData;
        }
        return $listOfProjectAdjustment;
    }

    public static function getProjectBasicDetails($tpid) {
        $resultObj = projectcontrolqueries::getTpbyTpid($tpid);
        $projectDetails = array();
        $customerAlias = reportsuite::customerAlias($tpid);
        $projectDetails['tpid'] = $tpid;
        $projectDetails['projectName'] = $resultObj->customer;
        $projectDetails['customerAlias'] = $customerAlias['name'];
        $projectDetails['jenkinsJobPath'] = $customerAlias['jenkinsJob'];
        if (isset($customerAlias['scriptPrefix']))
            $projectDetails['scriptPrefix'] = $customerAlias['scriptPrefix'];
        $projectDetails['customerCode'] = $resultObj->customer_code;
        $projectDetails['pos'] = $resultObj->pos;
        $projectDetails['autoEng'] = $resultObj->automation_engineer;
        $projectDetails['test_lead'] = $resultObj->test_lead;
        return $projectDetails;
    }

    public static function getProjectConfigurationDetail($customerAlias) {

//      drupal_set_message($_SERVER["SERVER_ADDR"]);
//        $customer= str_replace('SOAPUI', '', $customerAlias);
//        $projectConfig = parse_ini_file('/var/www/html/drupal/sites/all/modules/00007167/' .
//                'TestAutomationFramework/customerSettingsTAF/PST/' .
//                str_replace(" ", "", $customer) . '.properties');
//        
        $projectConfig = parse_ini_file('sites/all/modules/automation/' .
                'TestFramework/customerSettingsTAF/PST/' .
                str_replace(" ", "", $customerAlias) . '.properties');

        // Get from the customerSettingsTAF
        return $projectConfig;
    }

    public static function getReleaseNameFromJenkins($url) {

        exec("wget -O - '$@'  " . $url . " 2>&1", $shellOut);
        $releaseName = '';

        foreach ($shellOut as $stdout) {
            if (preg_match('/.zip/', $stdout)) {
                $input = substr($stdout, strpos($stdout, '<input') + 6, strpos($stdout, '/>'));
                $inputAttr = explode(" ", $input);
                foreach ($inputAttr as $attr) {
                    if (preg_match('/value=/', $attr)) {
                        $value = explode("=", $attr);
                        $releaseName = str_replace('"', "", $value[1]);
                    }
                }
            }
        }
        return $releaseName;
    }

    public static function executeJenkinsRun($url, $buildWithParamters) {
        $urlPost = '';
        $urlPost .= 'token=' . self::$jenkinsToken;

        $urlPost .= '&';

        foreach ($buildWithParamters as $key => $params) {
            if ($key === 'envIp')
                $urlPost .= 'environmentAddress=' . $params . '&';
            else if ($key === 'releaseName')
                null;
            else
                $urlPost .= $key . '=' . $params . '&';
        }

        $urlPost = substr($urlPost, 0, -1);

        //drupal_set_message($url.$urlPost);

        $wgetReq = $url . $urlPost;

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $wgetReq);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_exec($ch);
        curl_close($ch);
    }

    public static function cvsUpdateFramework($specific = null, $tpid = null) {
        global $user;
        $oldPath = getcwd();
        //drupal_set_message($oldPath);
        chdir(getcwd() . '/sites/all/modules/automation/TestFramework/TestSuites/');

        switch ($specific) {
            case 'TestScenarios':
                chdir("Test Scenarios");
                //exec("cvs -d :pserver:pst.automation:stemu4Ed@cvsserver.datalex.ie:/src/cvs update 2>&1 -d", $updateFm);
                exec("git pull 2>&1", $updateFm);
                break;
            case 'TestSuite':
                chdir("TestSuites");
                //exec("cvs -d :pserver:pst.automation:stemu4Ed@cvsserver.datalex.ie:/src/cvs update 2>&1 -d", $updateFm);
                exec("git pull 2>&1", $updateFm);
                break;
            case 'TestData':
                chdir("Configurations/Pages");
                //exec("cvs -d :pserver:pst.automation:stemu4Ed@cvsserver.datalex.ie:/src/cvs update 2>&1", $updateFm);
                exec("git pull 2>&1", $updateFm);
                break;
            case 'Configurations':
                chdir("customerSettingsTAF");
                //exec("cvs -d :pserver:pst.automation:stemu4Ed@cvsserver.datalex.ie:/src/cvs update PST 2>&1", $updateFm);
                exec("git pull 2>&1", $updateFm);
                break;
            default:
                //exec("cvs -d :pserver:pst.automation:stemu4Ed@cvsserver.datalex.ie:/src/cvs update 2>&1", $updateFm);
                exec("git pull 2>&1", $updateFm);
                break;
        }

        chdir($oldPath);
        if ($user->uid == 1)
            drupal_set_message('<pre>' . print_r($updateFm, 1) . '</pre>');
        if($specific == "TestScenarios") projectcontrolscripthandler::convertScriptDataToJSON($tpid);
        return;
    }

    public static function modifySettingsPropertyFile($customer, $properties) {
        $propsFile = projectcontrol::$pathToFramework .
                '/customerSettingsTAF/PST/' . $customer . '.properties';

        $fh = fopen($propsFile, 'w') or die("can't open file");

        $propData = "";
        foreach ($properties as $propKey => $propValue) {
            $propData .= $propKey . '=' . $propValue . "\n";
        }

        fwrite($fh, $propData);
        fclose($fh);
    }

    public static function commitTestSuiteChanges($cvsData, $customer, $isRerun = false) {
        global $user;
        $testSuiteFile = 'TestSuite' . str_replace(' ', '', $customer) . '.xls';
        $oldPath = getcwd();
        $commitFm = array();

        $rrAddit = $isRerun?'/rerun':'';

        //drupal_set_message($oldPath);
        $testSuiteDirPath = getcwd() . '/sites/all/modules/00007167/TestAutomationFramework/TestSuites'.$rrAddit;
        if (file_exists($testSuiteDirPath . '/' . $testSuiteFile) && user_is_logged_in()) {
            
            chdir($testSuiteDirPath);
            exec('cvs -d :pserver:pst.automation:stemu4Ed@cvsserver.datalex.ie:/src/cvs commit -m "' .
                    self::$JIRAPlaceholder . ' ' . $user->mail . ' made selection changes to cases" 2>&1', $commitFm);
            
            
            exec('whoami 2>&1', $agrg);
            drupal_set_message('<pre>' . print_r($agrg, 1) . '</pre>');
            
            drupal_set_message($testSuiteDirPath . '/' . $testSuiteFile);
        }
        if ($user->uid == 1)
            drupal_set_message('<pre>' . print_r($commitFm, 1) . '</pre>');

        chdir($oldPath);
    }

    public static function commitTestDataChanges($cvsData, $customerCode) {
        global $user;
        $testDataFile = 'Pages_' . $customerCode . '.xml';
        $oldPath = getcwd();
        $commitFm = array();
        $testDataDirPath = getcwd() . '/' . self::$pathToFramework . '/' . projectcontroltestdata::$pathToTestData;

        if (file_exists($testDataDirPath . '/' . $testDataFile) && user_is_logged_in()) {
            chdir($testDataDirPath);
            exec('cvs -d :pserver:pst.automation:stemu4Ed@cvsserver.datalex.ie:/src/cvs commit -m "' .
                    self::$JIRAPlaceholder . ' ' . $user->mail . ' made changes to test data" 2>&1', $commitFm);
        }
        if ($user->uid == 1)
            drupal_set_message('<pre>' . print_r($commitFm, 1) . '</pre>');

        chdir($oldPath);
    }

    public static function setPredefineCustomerSettings() {
        return array('releasePhase' => '',
            'testCycle' => '',
            'runByUser' => '',
            'reRunId' => '',
            'runType' => '',
            'tailMatrixTDP' => '',
            'soapPort' => '',
            'soapUrl' => '',
        );
    }
    
    public static function getRandomEasterEgg($eggType) {
        $retVal = '';
        switch ($eggType) {
            case 'devopsReaction':
                $eastEggs = self::easterEggDevOpsReaction();
                $maxEggs = count($eastEggs);
                $ri = rand(0, $maxEggs);
                $retVal .= '<h1><b><u>' . $eastEggs[$ri]['title'] . '</u></b></h1>';
                $retVal .= $eastEggs[$ri]['imgSrc'];
                $retVal .= '<br /> <small><a href="http://devopsreactions.tumblr.com">
                            *Courtesy from DevOps Reactions</a></small>';
                break;
            default:
                break;
        }

        return $retVal;
    }

    public static function easterEggDevOpsReaction() {
        $devOpsReactions = array(
            array(
                'title' => 'When you had enough typing...',
                'imgSrc' => '<img src="http://24.media.tumblr.com/tumblr_lwgme4IN1C1r6vj6fo1_500.gif">',
            ),
            array(
                'title' => 'When you ask PST A-Team about next year\'s plans during the yearly review',
                'imgSrc' => '<img src="http://media.tumblr.com/682e676127eb5dc8a1787262741bc5ad/tumblr_inline_mpulovlWvd1qz4rgp.gif">',
            ),
            array(
                'title' => 'When its finally working',
                'imgSrc' => '<img src="http://media.tumblr.com/772a9c560f0864c4d89414541ae835ef/tumblr_inline_mpl48vJTUP1qz4rgp.gif">',
            ),
            array(
                'title' => 'Fixing Bugs',
                'imgSrc' => '<img src="http://media.tumblr.com/2666f75887c09590958eda247c8f15e8/tumblr_inline_mpl3opPOYZ1qz4rgp.gif">',
            ),
            array(
                'title' => 'Protecting your servers during a DDoS',
                'imgSrc' => '<img src="http://i.imgur.com/LI3vaSs.gif">',
            ),
            //5
            array(
                'title' => 'When you find a stackoverflow question about your problem and there is no response',
                'imgSrc' => '<img src="http://media.tumblr.com/7c70d884a8ff2e7227fd6cda22ebbb7c/tumblr_inline_moij1tHWwa1qz4rgp.gif">',
            ),
            array(
                'title' => 'Explaining the chain of events that led to the outage',
                'imgSrc' => '<img src="http://media.tumblr.com/420b2da635fefe3717f5df69edf4f3d7/tumblr_inline_moiiyv7qNR1qz4rgp.gif">',
            ),
            array(
                'title' => 'Submitting an idea to the Architect',
                'imgSrc' => '<img src="http://i.imgur.com/BEziHmi.gif">',
            ),
            array(
                'title' => 'Exited editor without saving',
                'imgSrc' => '<img src="http://i.imgur.com/Z9tbQJF.gif">',
            ),
            array(
                'title' => 'When someone converted a python script to a shell script',
                'imgSrc' => '<img src="http://media.tumblr.com/85f122c721f8ec2bc0dbc2bdba521683/tumblr_inline_moihssJm1K1qz4rgp.gif">',
            ),
            //10
            array(
                'title' => 'When PST calls in the Automation team',
                'imgSrc' => '<img src="http://i.imgur.com/ymq7aZ4.gif">',
            ),
            array(
                'title' => 'PST Automation testing tools in action',
                'imgSrc' => '<img src="http://i.imgur.com/uM8s3gQ.gif">',
            ),
            array(
                'title' => 'Reaching a dynamic mac learn limit of 8192',
                'imgSrc' => '<img src="http://media.tumblr.com/b5e946fd3c44fb224930594d79dc4e32/tumblr_inline_mo94a32ccO1qz4rgp.gif">',
            ),
            array(
                'title' => 'Just when you thought you finished working on a project',
                'imgSrc' => '<img src="http://i.imgur.com/x0w5Urk.gif">',
            ),
            array(
                'title' => 'Writing unit tests',
                'imgSrc' => '<img src="http://media.tumblr.com/83ff10216be11cb3c8fcd2fd3d230d45/tumblr_inline_mnzt2ckrie1qz4rgp.gif">',
            ),
            array(
                'title' => 'I don’t need this piece of code',
                'imgSrc' => '<img src="http://i.imgur.com/8bLC92A.gif">',
            ),
            array(
                'title' => 'Deploying on a Friday night',
                'imgSrc' => '<img src="http://media.tumblr.com/470725895259732dbb2b4a46f64b6280/tumblr_inline_mncwrgZ79k1qz4rgp.gif">',
            ),
            array(
                'title' => 'When someone mentions he suspects silent disk corruption',
                'imgSrc' => '<img src="http://media.tumblr.com/d2d67bb7def2a454a9246601d34f94e0/tumblr_inline_mmrcbkkYRo1qz4rgp.gif">',
            ),
            array(
                'title' => 'One of those days…',
                'imgSrc' => '<img src="http://media.tumblr.com/0ae4384d84911fedebc8069b8c64df10/tumblr_inline_mmrba8r6oA1qz4rgp.gif">',
            ),
            array(
                'title' => 'After 6 hours at the data center',
                'imgSrc' => '<img src="http://media.tumblr.com/a4faeab51fcc5c08e026fed4b3d4c9ed/tumblr_inline_mmcpoz25Jy1qz4rgp.gif">',
            ),
            array(
                'title' => 'try / catch',
                'imgSrc' => '<img src="http://i.imgur.com/ZX5gQ.gif">',
            ),
            array(
                'title' => 'Codebase just before release',
                'imgSrc' => '<img src="http://media.tumblr.com/a967e00d148c2584bae1c73481a03a68/tumblr_inline_mmcoy5Kvz01qz4rgp.gif">',
            ),
            array(
                'title' => 'Go live day',
                'imgSrc' => '<img src="http://media.tumblr.com/ad4b6184cd13b9030d26ebec6f9134a9/tumblr_inline_mloizfQcoN1qz4rgp.gif">',
            ),
            array(
                'title' => 'When a recruiter sees “Automation" on your CV',
                'imgSrc' => '<img src="http://media.tumblr.com/97397c17eafcd75b47761144c1b93bbc/tumblr_inline_mloihw93Al1qz4rgp.gif">',
            ),
            array(
                'title' => 'When caffeine stops working',
                'imgSrc' => '<img src="http://i.imgur.com/K2aq9.gif">',
            ),
            array(
                'title' => 'Whole suite of automated tests passed',
                'imgSrc' => '<img src="http://media.tumblr.com/7347acfc654db520a5617d3e4fffd3b9/tumblr_inline_ml4701KEWE1qz4rgp.gif">',
            ),
            array(
                'title' => 'I don’t need to test that. What can possibly go wrong?',
                'imgSrc' => '<img src="http://i.imgur.com/43sf9NL.gif">',
            ),
            array(
                'title' => 'QA’s first look at new build',
                'imgSrc' => '<img src="http://i.imgur.com/VL5AN4g.gif">',
            ),
            array(
                'title' => 'The slashdot effect',
                'imgSrc' => '<img src="http://media.tumblr.com/20b911aae2be3a9172800c518b8a98c0/tumblr_inline_mk6vw92JW51qz4rgp.gif">',
            ),
            array(
                'title' => 'Trying complex code you see on websites',
                'imgSrc' => '<img src="http://media.tumblr.com/82715e2d6552ca5b3b2d20cefcdc99cd/tumblr_inline_mqilb78pyf1qz4rgp.gif">',
            ),
            array(
                'title' => 'QA gets Hold of the Latest Release',
                'imgSrc' => '<img src="http://i.imgur.com/29Olztd.gif">',
            ),
        );
        
        //Add Random From RSS dev opts reaction feed
        $xml = file_get_contents(
                 'http://devopsreactions.tumblr.com/rss',
                 false,
                 stream_context_create(array('http' => array('header' => 'Accept: application/xml')))
                );
        
        $xmlObj = simplexml_load_string($xml);
        
        foreach($xmlObj->channel->item as $item)
        {
            $devOpsReactions[] = array(
                'title' => (String)$item->title,
                'imgSrc' => (String)$item->description,
            );
        }
        
       return $devOpsReactions;
    }

}

?>
