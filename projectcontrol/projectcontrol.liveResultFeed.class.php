<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of projectcontrol
 *
 * @author jossie.saul
 */
class projectcontrolliveresultfeed {
    //put your code here
    
    public static function displayFeedTable($tpid){
        
        $lrfObj = projectcontrolqueries::getResultFeedForTpid($tpid);
        
        foreach($lrfObj as $lrf){
            $rows[] = array((String) $lrf->testcase_id, self::statusIntepretation($lrf->status));
        }
        $header = array('Test Cases', 'Status');
        return theme('table', array ('id' => 'liveFeed', 'header' => $header, 'rows' => $rows));
    }
    
    
    public static function intepretStatusCode($status){
        
    }
    
    public static function getAllSetTestCases($projectConfig, $projectInfo, $testScript){
        
        $refinedTCList = array();
        
        $activeCases = projectcontrolscripthandler::getActiveCasesByScenario(
                        $projectConfig['testSuite'], 
                        str_replace(' ', '', $projectInfo['customerAlias']));
       $filteredScenarioAndCases = projectcontrolscripthandler::
                filterTestScriptToScenarioAndCases($testScript);
        foreach($activeCases as $scenario => $cases){
            if($cases == "ALLACTIVE")
                $activeCases[$scenario] = $filteredScenarioAndCases[$scenario];
        }
        return $activeCases;
    }
    
    public static function prepareLiveFeedRunTime($customer, $tpid, $testCases = array()){
        
        projectcontrolqueries::clearLiveFeedResultsOnTpid($tpid);
        
        
        projectcontrolqueries::setupTestCaseOnRunRequest(
                self::refinedTestCaseListForDBEntry($customer, $testCases, $tpid), $tpid);
        
    }
    
    public static function refinedTestCaseListForDBEntry($customer, $testCases, $tpid){
        $caseList = array();
        
        foreach($testCases as $scenario => $cases){
            foreach($cases as $case){
                
                array_push($caseList, 
                        array('tpid' => $tpid, 
                              'testcase_id' => $customer.'\\'.$scenario.'\\'.$case,
                              'status' => 4,
                            ));
            }
        }
        return $caseList;
    }
    
    
    public static function statusIntepretation($status){
        $outcome = '';
        switch($status){
            case 0:
                $outcome = 'Failed';
                break;
            case 1:
                $outcome = 'Passed';
                break;
            case 2:
                $outcome = 'Not Executed';
                break;
            case 3;
                $outcome = 'Warning';
                break;
            case 4:
                $outcome = 'In Progress';
                break;
            default:
                $outcome = 'Not Ran';
                break;
        }
        return $outcome;
    }
}
