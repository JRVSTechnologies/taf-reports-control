<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of projectcontrol
 *
 * @author jossie
 */
class projectcontrolscripthandler 
{
    
    public static $testSuiteFolder = 'TestSuites';
    
    public static function getScriptByCustomer($projectDetails)
    {
        
        if($projectDetails['tpid'] == 52){
            return;
        }
        
        if(isset($projectDetails['scriptPrefix']))
            $scriptPrefix = $projectDetails['scriptPrefix'];
        else
            $scriptPrefix = str_replace(" ", "", $projectDetails['customerAlias']);
        
        $customerName = $scriptPrefix;
        $jsonFile = 'TestScriptData'.$customerName.'.json';
        $jsonFileLocation = projectcontrol::$pathToFramework.'/Test Scenarios/'.$jsonFile;
       
        //one time action
        if(!file_exists($jsonFileLocation)){
            self::convertScriptDataToJSON ($projectDetails['tpid']);
        }
            
        
        return json_decode(file_get_contents($jsonFileLocation), true);
    }
    
    public static function convertScriptDataToJSON($tpid)
    {
        drupal_set_message("Creating List of TestCases");
        $projectDetails = projectcontrol::getProjectBasicDetails($tpid);
        if(in_array($projectDetails['tpid'], array('10')))
                return;
        
        if(isset($projectDetails['scriptPrefix']))
            $scriptPrefix = $projectDetails['scriptPrefix'];
        else
            $scriptPrefix = str_replace(" ", "", $projectDetails['customerAlias']);
        
        $customerName = $scriptPrefix;
        //Copa bug temp fix
        if($tpid != 29)
            $scriptPrefix .= '_';
        
        $scriptsData = array();
        
        if($handle = opendir(projectcontrol::$pathToFramework.'/Test Scenarios/'.$customerName))
        {
            while (false !== ($entry = readdir($handle))) 
            {
                if(preg_match('/^'.$scriptPrefix.'/', $entry) && !preg_match('/AdditionalSteps/', $entry)
                        && !preg_match('/.csv/', $entry) && !preg_match('/SOAPUI/', $entry))
                $scriptsData[$entry] = self::getDataFromScript($entry, $customerName);
            }
        }
        
        $jsonFile = 'TestScriptData'.$customerName.'.json';
        $jsonFileLocation = projectcontrol::$pathToFramework.'/Test Scenarios/'.$jsonFile;
        if(file_exists($jsonFileLocation)) 
        {
            drupal_set_message("File Deleted to renew");
            unlink ($jsonFileLocation);
        }
        $json = json_encode($scriptsData);
        //if($user->uid == 1) drupal_set_message('<pre>' . print_r($scriptData, 1) . '</pre>');
        file_put_contents($jsonFileLocation, $json);
        return $scriptsData;
    }
    
    protected static function getDataFromScript($filename, $customerName = null)
    {
        $pathSuffix = $customerName==null?'':$customerName.'/';
        $objPHPExcel = PHPExcel_IOFactory::load(
                    projectcontrol::$pathToFramework.'/Test Scenarios/'.$pathSuffix.
                    $filename);
        $scriptData = array('Parameters' => array(), 
                            'TestCases' => array(),
                           );
        
        $rawData = $objPHPExcel->setActiveSheetIndexByName('TestCases')->toArray(NULL, TRUE, TRUE, TRUE);
        
        $caseScriptFlow = false;
        
        foreach ($rawData as $key => $data)
        {
            //drupal_set_message('<pre>' . print_r($data, 1) . '</pre>');
            if($data['A'] === 'Assign')
            {
                $scriptData['Parameters'][] = array('name' => $data['A'],
                                                    'data#1' => $data['B'],
                                                    'data#2' => $data['C'],
                                                    'data#3' => $data['D'],
                                            );
            }
            
            if($data['A'] == 'TestCase')
            {
                $scriptData['TestCases'][$data['C']]['name'] = $data['C'];
                $scriptData['TestCases'][$data['C']]['desc'] = $data['B'];
                $tcID = $data['C'];
                $caseScriptFlow = true;
            }
            
            if($data['A'] == 'Load')
            {
                $sheetName = str_replace("From table=", "", $data['C']);
                $scriptData['TestCases'][$tcID]['dataTable'] = $sheetName;
                        
                $scriptData['TestCases'][$tcID]['numOfRows'] =
                        $objPHPExcel->setActiveSheetIndexByName($sheetName)->getHighestDataRow();
            }
            
            if($data['A'] == 'Action')
            {
                $scriptData['TestCases'][$tcID]['script'][] = 
                    array('action' => $data['B'],
                          'data#1' => $data['C'],
                          'data#2' => $data['D'],
                          'data#3' => $data['E'],
                          'data#4' => $data['F'],
                          'data#5' => $data['G'],
                          'comment' => $data['H'],
                          'description' => $data['I'],
                          'ExpectedResults' => $data['J'],
                        );
            }
        }
        
        //drupal_set_message('<pre>' . print_r($scriptData, 1) . '</pre>');
        return $scriptData;
    }
    
    public static function getActiveCasesByScenario($filename, $customer = null)
    {
        
        $objPHPExcel = PHPExcel_IOFactory::load(
                    projectcontrol::$pathToFramework.'/TestSuites/'.
                    $filename);
        $suiteData = $objPHPExcel->getActiveSheet()->toArray(NULL, TRUE, TRUE, TRUE);
        
        $activeCases = array();
        $cycle = 1;
        foreach ($suiteData as $key => $data)
        {
            if($cycle > 1 && isset($data['A']))
            {
                $scenario = self::removeFileExtensionString($data['B']);
                $scenario = str_replace($customer.'\\', '', $scenario);
                
                if(!empty($data['C']))
                    $activeCases[$scenario] = explode(',', $data['C']);
                else
                    $activeCases[$scenario] = 'ALLACTIVE';
            }
            
            $cycle++;
        }
        return $activeCases;
    }
    
    public static function setScenarioCasesToTestSuite($customer, $selectedCases, $folderPrefix = null)
    {
        $customer = str_replace(" ", "", $customer);
        
        //Used to set which row to insert
        $rowOffset = 2;
        //Used to guide which actual row it was
        $rowOffsetGuide = 2;
   
        
        $configCell = 'A';
        $scenarioCell = 'B';
        $testCasesCell = 'C';
             
        
      $excRead = new PHPExcel_Reader_Excel5();
      
      $excel2 = $excRead->load(projectcontrol::$pathToFramework.'/TestSuites/TestSuite'.
                    $customer.'.xls');
      $excel2->setActiveSheetIndex(0);
      
      //$excel2->setFormatCode(PHPExcel_Cell_DataType::TYPE_STRING);
//        $excel2->getActiveSheet()->setCellValue('A1', 'Configuration')
//                    ->setCellValue('B1', 'ScenarioName')       
//                    ->setCellValue('C1', 'TestCases');
     
        foreach ($selectedCases as $scriptName => $data)
        {
             $excel2->getActiveSheet()->setCellValue('A'.$rowOffsetGuide, '')
                    ->setCellValue('B'.$rowOffsetGuide, '')       
                    ->setCellValue('C'.$rowOffsetGuide, '');
//             
            //No Cases Selected for this
            if($data['selectedCasesCount'] === 0)
            {
                
            }
            //All cases have been instructed to run
            else if($data['selectedCasesCount'] === $data['numberOfTotalCases'])
            {
                //Nothing to put in Cell C
                $testCases = '';
            }
            // Only a few cases have been selected to Run
            else if($data['selectedCasesCount'] < $data['numberOfTotalCases'])
            {
                $testCases = implode(',', $data['selectedCases']);
            }
            
            
            if($data['selectedCasesCount'] !== 0)
            {
                $excel2->getActiveSheet()->setCellValue('A'.$rowOffset, 'Default')
                       ->setCellValue('B'.$rowOffset, $scriptName.'.xls')       
                       ->setCellValue('C'.$rowOffset, $testCases);
            
                $rowOffset++;
            }
            
            $rowOffsetGuide++;
            
        }
        
        $objWriter = PHPExcel_IOFactory::createWriter($excel2, 'Excel5');
        //$objWriter = new PHPExcel_Writer_Excel5_BIFFwriter($excel2);
        //$objWriter->setOffice2003Compatibility(true);
        $objWriter->save(projectcontrol::$pathToFramework.'/TestSuites/TestSuite'.
                    $customer.'.xls');
        
    }
    
    public static function setScenarioCasesToTestSuiteJson($customer, $selectedCases, 
        $isRerun = false)
    {
        $customer = str_replace(" ", "", $customer);
        $jsonFile = 'TestSuite'.$customer.'.json';
        $rrAddon = $isRerun?'rerun/':'';
        
        $jsonFileLocation = projectcontrol::$pathToFramework.'/TestSuites/'.$rrAddon.$jsonFile;
        
        $jsonArr = array();
        
        foreach ($selectedCases as $scriptName => $data)
        {
            //No Cases Selected for this
            if($data['selectedCasesCount'] === 0)
            {
                
            }
            //All cases have been instructed to run
            else if($data['selectedCasesCount'] === $data['numberOfTotalCases'])
            {
                //Nothing to put in Cell C
                $testCases = '';
                $jsonArr['TestSuite']['Scenario'][] = array(
                  'name' => $customer.'\\'.$scriptName,
                  'fileType' => 'xls',
                  'Configuration' => 'Default',
                  'TestCases' => array(
                      'Case' => 'ALL'
                  )
                );
            }
            // Only a few cases have been selected to Run
            else if($data['selectedCasesCount'] < $data['numberOfTotalCases'])
            {
                $testCases = implode(',', $data['selectedCases']);
                $jsonArr['TestSuite']['Scenario'][] = array(
                  'name' => $customer.'\\'.$scriptName,
                  'fileType' => 'xls',
                  'Configuration' => 'Default',
                  'TestCases' => array(
                      'Case' => $data['selectedCases'],
                  )
                );
            }
        }
        $json = json_encode($jsonArr);
        file_put_contents($jsonFileLocation, $json);
    }
    
    public static function convertTestSuiteJsonToXls($customer, $isRerun = false)
    {
        global $user;
        $oldPath = getcwd();
        $rrAddit = $isRerun?'true':'false';
        //chdir(getcwd().'/sites/all/modules/00007167/TestAutomationFramework');
        chdir(getcwd().'/sites/all/modules/automation/TestFramework/');
        //chdir(getcwd().projectcontrol::$pathToFramework);
        $customer = str_replace(" ", "", $customer);
        $cmd = "java -jar selenium-taf/selenium-taf.jar -testSuiteUtility convertJsonToXls ".$rrAddit.
            " ".$customer." 2>&1";
        //drupal_set_message($cmd);
        exec($cmd, $execOutput);
        chdir($oldPath);
        if($user->uid == 1)
        drupal_set_message('<pre>' . print_r($execOutput, 1) . '</pre>');
    }
    
    public static function filterTestScriptToScenarioAndCases($testScript){
        foreach ($testScript as $scenario => $script){
            $scenario = self::removeFileExtensionString($scenario);
            $filteredScenarioAndCases[$scenario] = array();
            foreach($script['TestCases'] as $testCase => $caseDt){
                
                if($caseDt['numOfRows'] > 2){
                    for($i = 1; $i < $caseDt['numOfRows']; $i++){
                        array_push($filteredScenarioAndCases[$scenario], 
                                $testCase.'_Data_'.$caseDt['dataTable'].
                                '_Row_'.$i);
                    }
                }else{
                     array_push($filteredScenarioAndCases[$scenario], $testCase);
                }
            }
        }
        //drupal_set_message('<pre>' . print_r($filteredScenarioAndCases, 1) . '</pre>');
        return $filteredScenarioAndCases;
    }
    
    public static function changeTestSuiteFromRESTRQ(){
        
    }
    
    public static function removeFileExtensionString($scenario)
    {
        return str_replace(".xls", '', $scenario);
    }
    
    public static function getNumberOfRowsFromDataTable(){
        
    }
    
    public static function getScriptToRunStatus()
    {
        
    }
}

?>
