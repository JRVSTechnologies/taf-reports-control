<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of projectcontrol
 *
 * @author jossie
 */
class projectcontroltestdata {
    
    
    public static $pathToTestData = 'Configurations/Pages/';
    
    public static $tdFilePrefix = 'Pages_';
    
    public static $tdFileExtension = '.xml';
    
    
    
    public static function elementAttributes()
    {
        return array('name', 'value', 'locator', 'action', 'isCommon', 'isMandatory');
    }
    
    public static function processTestDataFile($customerCode)
    {
        $file = projectcontrol::$pathToFramework.'/'.self::$pathToTestData.
                        self::$tdFilePrefix.$customerCode.self::$tdFileExtension;
        
        if(file_exists($file))
            return simplexml_load_file($file);
        else
            drupal_set_message("File does not exist", 'error');
        
        return;
    }
    
    public static function organiseTestDataStructure($tdObj)
    {
        $pages = 'TESTDATA:Pages';
         // drupal_set_message('<pre>' . print_r($testData, 1) . '</pre>');
          foreach($tdObj as $objHL)
          {
              //drupal_set_message('<pre>' . print_r($objHL, 1) . '</pre>');
              $pageName = 'PAGE:'.(String)$objHL->attributes()->name;
              $testData[$pages][$pageName] = array();
              
              foreach ($objHL->Pos as $objLWpos)
              {
                  $PoS = 'POS:'.(String)$objLWpos->attributes()->code;
                  $testData[$pages][$pageName][$PoS] = array();
                  
                  if (isset($objLWpos->Traveller))
                  {
                      foreach ($objLWpos->Traveller as $traveller)
                      {
                          $travellerNo = (String)$traveller->attributes()->amount;
                          foreach($traveller as $element)
                            $testData[$pages][$pageName][$PoS]['Traveller']
                                     [$travellerNo][] = (Array)$element->attributes();
                          
                      }
                  }
                  else
                  {
                      foreach ($objLWpos->Element as $element)
                      {
                          $testData[$pages][$pageName][$PoS][] = (Array)$element->attributes();
                      }
                  }
              }
          }
          return $testData;
    }
    
    /**
     * 
     * DataSet
     * -action
     * -isCommon
     * -isMandatory
     * -locator
     * -name
     * -value
     * 
     * @param type $tdObj
     * @param type $SelectedPoS
     * @return string
     */
    public static function processIntoTable($tdObj, $SelectedPoS)
    {
        $tablePackage = array();
        
        foreach ($tdObj['TESTDATA:Pages'] as $page => $posData)
        {
            //Skip to next loop if doesn't exist
            if(!isset($posData['POS:'.$SelectedPoS])) continue;
            if($SelectedPoS !== NULL) $posData = $posData['POS:'.$SelectedPoS];
            $tablePackage[$page] = '';
            $tablePackage[$page] .= '<table>';
            $tablePackage[$page] .= '<tr><th colspan="6">';
            $tablePackage[$page] .= '<a id="'.$page.'">';
            $tablePackage[$page] .= str_replace('PAGE:', '', $page);
            $tablePackage[$page] .= '</a>';
            $tablePackage[$page] .= '</th></tr>';
            
            $tablePackage[$page] .= '<tr>';
            $tablePackage[$page] .= '<th>Form Element</th>';
            $tablePackage[$page] .= '<th>Value</th>';
            $tablePackage[$page] .= '<th>Locator</th>';
            $tablePackage[$page] .= '<th>Action</th>';
            $tablePackage[$page] .= '<th>Common</th>';
            $tablePackage[$page] .= '<th>Mandatory</th>';
            $tablePackage[$page] .= '</tr>';
            //drupal_set_message('<pre>' . print_r($posData, 1) . '</pre>');
            
            $rowAdd = '';
            if(isset($posData['Traveller']))
            {
                foreach ($posData['Traveller'] as $traveller => $dataSet)
                {
                    $rowAdd .= '<tr><th>Traveller - '.
                                $traveller.'</th></tr>';
                    foreach ($dataSet as $data)
                    {
                        $attr = $data['@attributes'];
                        $rowAdd .= '<tr>';
                        $rowAdd .= '<td>'.$attr['name'].'</td>';
                        $rowAdd .= '<td>'.$attr['value'].'</td>';
                        $rowAdd .= '<td>'.$attr['locator'].'</td>';
                        $rowAdd .= '<td>'.$attr['action'].'</td>';
                        $rowAdd .= '<td>'.$attr['isCommon'].'</td>';
                        $rowAdd .= '<td>'.$attr['isMandatory'].'</td>';
                        $rowAdd .= '</tr>';
                    }
                }
            }
            else
            {
                foreach ($posData as $data)
                {
                    $attr = $data['@attributes'];
                    $rowAdd .= '<tr>';
                    $rowAdd .= '<td>'.$attr['name'].'</td>';
                    $rowAdd .= '<td>'.$attr['value'].'</td>';
                    $rowAdd .= '<td>'.$attr['locator'].'</td>';
                    $rowAdd .= '<td>'.$attr['action'].'</td>';
                    $rowAdd .= '<td>'.$attr['isCommon'].'</td>';
                    $rowAdd .= '<td>'.$attr['isMandatory'].'</td>';
                    $rowAdd .= '</tr>';
                  //drupal_set_message('<pre>' . print_r($data, 1) . '</pre>');
                }
            }
            
            $tablePackage[$page] .= $rowAdd;
            
            $tablePackage[$page] .= '</table>';
            
        }
        
        return $tablePackage;
    }
    
    /**
     * Function to get a list of pages parent elements of which handles a single UI Page
     * 
     * @param type $tdObj
     * @return type
     */
    public static function getListOfPagesFromTdObj($tdObj)
    {
        $arrOfPages = array();
        foreach ($tdObj['TESTDATA:Pages'] as $page => $posData)
        {
              $arrOfPages[$page] = str_replace('PAGE:', '', $page);
        }
        return $arrOfPages;
    }
    
    public static function getTestData($customerCode)
    {
        $testDataRaw = self::processTestDataFile($customerCode);
        
        return self::organiseTestDataStructure($testDataRaw);;
    }
    
    public static function setTestData($customerCode, $tdObj)
    {
        $tdObj->asXML(projectcontrol::$pathToFramework.'/'.self::$pathToTestData.
                        self::$tdFilePrefix.$customerCode.self::$tdFileExtension);
        
        if($_SERVER['SERVER_ADDR'] === '10.153.30.100')
            projectcontrol::commitTestDataChanges ($cvsData, $customerCode);
        
    }
    
    
}

?>
