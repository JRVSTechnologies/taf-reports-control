<?php


/**
 * Description of projectcontrol
 * Class of function for one off or utilities
 *
 * @author jossie
 */
class projectcontrolutilities {
    
    
    public static $UIConfigPolicy = 'UIConfiguration';
    public static $dlxCaptchaBeanCom = "com.datalex.tdp.ui.util.captcha.SimpleCaptchaBuilder";
    
    public static function verifyBeansPolicy($bean, $condition, 
            $params = array())
    {
        switch($bean)
        {
            case 'captcha':
            $beansObj = self::getBeansPolicy($params['envAddr']."/Tomcat/webapps/InternetBooking".
                        "/WEB-INF/classes/beanpolicies.cfg");
            
            if($condition === 'static')
            {
                if(!self::isCaptchaBeanStatic($beansObj))
                {
                    drupal_set_message("Captcha property in Tomcat beanpolicies have not been set to static.
                        Please set <b>characterSet</b> to <b>01</b> in the bean {com.datalex.tdp.ui.util.captcha.SimpleCaptchaBuilder}.
                        Follow the instruction by ".l("click here", 
                        "http://confluence.datalex.com/pages/viewpage.action?pageId=19413719"), "warning");
                    return false;
                }
            }
            break;
        }
        return true;
    }
    
    public static function getBeansPolicy($filePath)
    {
        $file = fopen($filePath, "r");
        if(!$file)
        {
            drupal_set_message('Unable to get beans policies file', 'error');
            return;
        }
        return simplexml_load_string(stream_get_contents($file));
        
    }
    
    private static function isCaptchaBeanStatic($beansObj)
    {
        foreach($beansObj as $policy)
        {
            if($policy->attributes()->id == self::$UIConfigPolicy)
            {
                foreach ($policy as $bean)
                {
                    if($bean->attributes()->impl == self::$dlxCaptchaBeanCom)
                    {
                        foreach($bean as $property)
                        {
                            if($property->attributes()->id == "characterSet" &&
                                    $property->attributes()->val == "01")
                            {
                                return true;
                            }
                        }
                    }
                }
            }
        }
        return false;
    }
    
    public static function setNotificationMailingList($projectName, $mailingList)
    {
        $customerNotificationPath = drupal_get_path('module', 'projectcontrol')
                .'/lib/notification/'.str_replace(' ', '', $projectName);
        if(!file_exists($customerNotificationPath))
        {
            if(mkdir($customerNotificationPath))
                drupal_set_message('Created new notification directory');
        }
        file_put_contents($customerNotificationPath.'/NotifyAfterCompletion.json',
                json_encode($mailingList));
        
    }
    
    public static function unsetNotificationMailingList($customer){
        $customerNotificationPath = drupal_get_path('module', 'projectcontrol')
                .'/lib/notification/'.str_replace(' ', '', $customer);
        
        if(file_exists($customerNotificationPath))
            unlink ($customerNotificationPath.'/NotifyAfterCompletion.json');
    }
}

?>
