<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of projectcontrol
 *
 * @author jossie
 */
class projectcontrolqueries extends reportsuitequeries 
{
    
    
    public static function getResultFeedForTpid($tpid){
        
        db_set_active('taf');
            $query = db_select('live_result_feed', 'lrf');
            $query->condition('lrf.tpid', $tpid)
                  ->fields('lrf');
            $resultObj = $query->execute();
        db_set_active();
        
        return $resultObj;
    }
    
    public static function setupTestCaseOnRunRequest($testCaseArray, $tpid){
        db_set_active('taf');
        $query = db_insert('live_result_feed')->fields(array('tpid', 'testcase_id', 'status'));
        
        foreach($testCaseArray as $record){
            $query->values($record);
        }
        $query->execute();
        db_set_active();
    }
    
    public static function clearLiveFeedResultsOnTpid($tpid){
        
        db_set_active('taf');
        
        $query = db_delete('live_result_feed')
                ->condition('tpid', $tpid)
                ->execute();
        
        db_set_active();
        
    }
    
    
    
}

?>
