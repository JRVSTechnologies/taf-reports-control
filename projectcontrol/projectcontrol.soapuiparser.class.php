<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of projectcontrol
 *
 * @author jossie.saul
 */
class projectcontrolsoapuiparser {
    //put your code here
    
    public static function loadProjectIntoXMLProject($projectName){
        
        //need to get the scripts
        $scriptName = '';
        if($handle = opendir(projectcontrol::$pathToFramework.'/Test Scenarios/'.$projectName))
        {
            while (false !== ($entry = readdir($handle)))
            {
                if(preg_match('/SOAPUI/', $entry))
                   $scriptName = $entry;
            }
        }
        //drupal_set_message(projectcontrol::$pathToFramework.'/Test Scenarios/'.$projectName.'/'.$scriptName);
        $rawFile = file_get_contents(projectcontrol::$pathToFramework.'/Test Scenarios/'.$projectName.'/'.$scriptName);
        
        $rawFile = str_replace("con:", "", $rawFile);
        
        //drupal_set_message($rawFile);
        $xmlObj = simplexml_load_string($rawFile, null, LIBXML_NOCDATA);
        //drupal_set_message('<pre>' . print_r($xmlObj->properties, 1) . '</pre>');
        
        return $xmlObj;
    }
    
    public static function getEnvironmentProperties($xmlObject)
    {
        //drupal_set_message('<pre>' . print_r($xmlObject->properties, 1) . '</pre>');
        return $xmlObject->properties;
    }
    
}
