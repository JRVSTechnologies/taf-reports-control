
<table>
    <tr style="background-color: white;">
        <td>
            <?php print $data['link']['run']['controlSuite']; ?>
            <br /><b>Run Automation</b>
        </td>
        <td style="text-align: right;">
            <?php print $data['link']['report']['controlSuite']; ?>
            <br /><b>Automation Test Report</b>
        </td>
    </tr>
</table>

 <fieldset class="collapsible collapsed"> 
    <legend><span class="fieldset-legend">Progress</span></legend> 
      <div class="fieldset-wrapper"> 
          
      <iframe src="<?php print $data['paths']['html']['jenkinsLastBuild']; ?>"
              frameborder="0" width="100%" scrolling="no">
      </iframe>
          
          
    </div>
 </fieldset>

<fieldset class="collapsible collapsed"> 
    <legend><span class="fieldset-legend">RealTime Test Result (BETA)</span></legend> 
      <div class="fieldset-wrapper"> 
        <?php print $data['liveFeedDataTable'] ?>
    </div>
 </fieldset>

 <fieldset class="collapsible uncollapsed"> 
    <legend><span class="fieldset-legend">Configuration</span></legend> 
      <div class="fieldset-wrapper"> 
          <p><?php //print $data['link']['cvsUpdate']['config']; ?></p>
          <p>
            <b>Environment: </b>
            <?php 
            
            $env = '';
            
            if(!in_array($data['projectConfig']['runMode'], 
                               array('SERVICE', 'SERVICEPARALLEL')))
            $env .= strtolower($data['projectConfig']['protocol']).'://';
            $env .= $data['projectConfig']['envIp'];

            print l($env, $env);

            ?>
          </p>
          <p>
            <b>Browser:</b> 
            <?php print $data['projectConfig']['browserType']; ?>
          </p>
          <p>
            <b>Run Mode:</b> <?php print $data['projectConfig']['runMode']; ?>
          </p>
          
          <p>
          <h3><u>Release Information</u></h3>
          <b>Release Phase:</b> 
              <?php if(!empty($data['projectConfig']['releasePhase'])){
                  print $data['projectConfig']['releasePhase']; 
              }?><br />
          <b>Test Cycle:</b> 
              <?php if(!empty($data['projectConfig']['testCycle'])){
                      print $data['projectConfig']['testCycle']; 
              }?><br />
          <b>Release Package:</b> 
              <?php if(!empty($data['projectConfig']['testCycle'])){
                  print $data['projectConfig']['releaseName'];
              }?><br />
          </p>
          
          <p>
          <h3><u>TAF Details:</u></h3>
          <b>Project Name:</b> <?php print $data['projectConfig']['projectName']; ?>
          <br />
          
          <b>POS:</b> <?php print $data['projectConfig']['pos']; ?>
          <br />
          
          <b>Customer Code:</b> <?php print $data['projectConfig']['customerCode']; ?>
          <br />
          <a href="http://confluence.datalex.com/display/PST/TAF+Configurations+and+Information">
              Additional Information
          </a>
          </p>
    </div>
 </fieldset>


 <fieldset class="collapsible collapsed"> 
    <legend><span class="fieldset-legend">Test Cases</span></legend> 
      <div class="fieldset-wrapper"> 
          <div class="messages warning">
              Click on the 'Update' link below - if you do not see the test cases you expected to see.
              <br />
              <?php print $data['link']['cvsUpdate']['testScripts']; ?>
          </div>
       <?php 
       if(isset($data['testScripts']))
       {
//            print '<span style="text-align:right">';
//            print $data['link']['cvsUpdate']['testScenario'];
//            print '</span>';
            
            print drupal_render(drupal_get_form('projectcontrol_testScript_form',
            $data['testScripts'], $data['projectDetails'], $data['projectConfig'])); 
       }
       else
       {
           print 'Test Script Configuration is unavailable for this project';
       }
            
       ?>
    </div>
 </fieldset>

 <fieldset class="collapsible collapsed"> 
    <legend><span class="fieldset-legend">Test Data</span></legend> 
      <div class="fieldset-wrapper"> 
       
          <?php
//          if(isset($data['testData']))
//          {
//              print '<span style="text-align:right">';
//              print $data['link']['cvsUpdate']['testData'];
//              print '</span>';
//              print '<a id="testDataTop"></a>';
//          }
          ?>
          <br />
          <center><h2>Pages</h2>
          <?php 
          if(isset($data['testData']))
          {
            $pagesList = projectcontroltestdata::
                         getListOfPagesFromTdObj($data['testData']['set']);
            foreach ($pagesList as $pageId => $pageTitle)
            {
                print '<a href="#'.$pageId.'">'.$pageTitle.'</a> | ';
            }
          }
          ?>
          </center>
          <br />
          <br />
          <?php
          if(isset($data['testData']))
          {
              foreach ($data['testData']['table'] as $pgId => $table)
              {
                  print '<table>';
                  print '<tr id="tblOverrideBg"><td style="text-align:left;">';
                  print '<a href="#testDataTop">Back To Top</a>';
                  print '</td>';
                  print '<td style="text-align:right;">';
                  print l($data['paths']['image']['edit']['testData'], 
                    'projectcontrol/editTestData/'.
                    $data['projectDetails']['tpid'].'/'.
                    $data['projectDetails']['customerCode'].'/'.
                    $pgId,
                    array('query' => drupal_get_destination(),
                          'html' => true
                         )
                        );
                  print '</td>';
                  print '</tr>';
                  print '</table>';
                  
                  print $table;
                  print '<br/ >';
              }
          }
          else
          {
              print 'Test Data Configuration is unavailable for this project';
          }
          
          ?>
          
    </div>
 </fieldset>


<div id="runAutoDialog" title="Run Automation">
    <p class="validateTips">All Form Fields are required</p>
    
    <?php
        print drupal_render(drupal_get_form('projectcontrol_configuration_form', 
                $data['projectDetails'],$data['projectConfig']));
    ?>
    
</div>