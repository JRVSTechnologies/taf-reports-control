<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ateamutils
 *
 * @author jossie.saul
 */
class ateamutilsqueries {
    //put your code here
    
    public static function getAllPortStatus(){
        db_set_active('taf');
            $query = db_select('hub_port_avail', 'hpa');
            $query->fields('hpa');
            $result = $query->execute();
       db_set_active();
       
       
       return $result;
    }
    
    
    public static function getFirstPortAvailOnHub(){
        db_set_active('taf');
            $query = db_select('hub_port_avail', 'hpa');
            $query->fields('hpa', array('port'))
                  ->condition('hpa.status', 0)
                  ->range(0,1);
            $result = $query->execute()->fetchField();
       db_set_active();
       
       
       return $result;
    }
    
    public static function setPortLock($port){
         db_set_active('taf');
            $query = db_update('hub_port_avail');
            $query->fields(array('status' => 1))
                        ->condition('port', $port, '=')
                        ->execute();
        db_set_active();
    }
    
    public static function setPortUnlock($port){
        db_set_active('taf');
            $query = db_update('hub_port_avail');
            $query->fields(array('status' => 0))
                        ->condition('port', $port, '=')
                        ->execute();
        db_set_active();
    }
}
