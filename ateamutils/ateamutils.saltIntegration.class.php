<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ateamutils
 *
 * @author jossie.saul
 */
class ateamutilssaltintegration {
    
    
    public static function determineEnvironmentToRunAutomationAgainst($xmlObj){
        //drupal_set_message('<pre>' . print_r($xmlObj->JenkinsJobRegister->JReg, 1) . '</pre>');
        //drupal_set_message('<pre>' . print_r($xmlObj, 1) . '</pre>');
        $jenkinsUrlExt = (String)$xmlObj->ProjectProperties['PortalProjectID']=='41'?
                '/lastBuild/api/xml':'/lastSuccessfulBuild/api/xml';
        $latestBuild = '';
        $lastTimeComparison = 0;
        foreach($xmlObj->JenkinsJobRegister->JReg as $jjr){
            drupal_set_message($jjr.$jenkinsUrlExt);
            $jenkinsXml = file_get_contents($jjr.$jenkinsUrlExt);
            $jXmlObj = simplexml_load_string($jenkinsXml);
            $timeBuildRanRaw = explode("_", $jXmlObj->id);
            //drupal_set_message($timeBuildRanRaw[0]." ".str_replace("-", ":", $timeBuildRanRaw[1]));
            $timeBuildRan = strtotime($timeBuildRanRaw[0]." ".str_replace("-", ":", $timeBuildRanRaw[1]));;
            //drupal_set_message($timeBuildRan);
            drupal_set_message($lastTimeComparison ."=". $timeBuildRan);
            if($lastTimeComparison < $timeBuildRan){
                $lastTimeComparison = $timeBuildRan;
                foreach($jXmlObj->action->parameter as $k => $param){
                    if((String)$param->name === 'environment'){
                        $latestBuild = (String)$param->value;
                        drupal_set_message((String)$param->value);
                    }
                }
            }
            
        }
        return $latestBuild;
        
    }
    
    public static function getEnvironmentPropertiesDetails($xmlObj, $envToGet)
    {
        //drupal_set_message('<pre>' . print_r($xmlObj, 1) . '</pre>');
        $environmentProp = array();
        foreach($xmlObj->Environment as $envProps){
            //drupal_set_message('<pre>' . print_r($envProps, 1) . '</pre>');
            //drupal_set_message($envProps['ID']);
            if($envProps['ID'] == $envToGet){
                drupal_set_message($envProps['ID'].' Found');
                $environmentProp = $envProps;
                break;
            }
        }
        drupal_set_message('<pre>' . print_r($environmentProp, 1) . '</pre>');
        
        return $environmentProp;
    }
    
    
    public static function getJenkinsRequiredParameters(){
        return array(
              'OverrideDefaultSettings' => true,
              'reRunId' => null,
              'runType' => '70',
              'protocol' => null,
              'envIp' => null,
              'browserType' => null,
              'runMode' => 'GRID',
              'tailMatrixTDP' => 'OFF',
              'releasePhase' => null,
              'testCycle' => null,
              'runByUser' => 'systemtest',
        );
    }
    
    
}
