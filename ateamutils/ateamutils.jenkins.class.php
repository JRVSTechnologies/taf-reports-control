<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ateamutils
 *
 * @author jossie
 */
class ateamutilsjenkins 
{
   
    public static $configBuilders = 'builders';
    
    public static function getJobConfigXml($jobLocation, $u, $p)
    {
        $putFileToPath = drupal_get_path('module', 'ateamutils').'/lib/tempConfigOnReq.xml';
        $cmd = "wget --auth-no-challenge --user=".$u." --password=".$p." -qO ".$putFileToPath." - ".$jobLocation."config.xml 2>&1";
        exec($cmd, $shellOut);
        
        return simplexml_load_string(file_get_contents($putFileToPath));
    }
    
    public static function getfilteredConfigurableParams($configXmlObj)
    {
        $configuration = array(
            'jobParameters' => array(
                'string' => array(),
                'select' => array(),
                'radio' => array(),
                'checkbox' => array(),
            )
        );
        
        $configuration['jobParameters']['string'] = 
            self::getHudsonModelStringParametersFromConfig($configXmlObj);
        
        $configuration['builders']['batchFiles']['cmd'] = 
            self::getBatchFileCommand(self::getBuildersFromConfig($configXmlObj));
        
        return $configuration;
    }
    
    public static function getHudsonModelStringParametersFromConfig($configXmlObj)
    {
        $properties = 'properties';

        $paramDefProperty = 'hudson.model.ParametersDefinitionProperty';

        $paramDef = 'parameterDefinitions';

        $stringParamDef = 'hudson.model.StringParameterDefinition';
    
        $confStringParams = array();
        foreach($configXmlObj->$properties->$paramDefProperty->$paramDef->$stringParamDef as $stringParams)
        {
            $confStringParams[] = $stringParams;
        }
        
        return $confStringParams;
    }
    
    public static function getBuildersFromConfig($configXmlObj)
    {
        return $configXmlObj->builders;
    }
    
    public static function setBuildersForConfig($configXmlObj, $cmd)
    {
        $batchfile = 'hudson.tasks.BatchFile';
        $configXmlObj->builders->$batchfile->command = $cmd;
    }
    
    public static function postModifiedConfigToJob($jenkinsJobUrl, $project)
    {
       
    }
    
    protected static function getBatchFileCommand($configXmlObj)
    {
        $hudsonTaskBatchFile = 'hudson.tasks.BatchFile';
        return (string)$configXmlObj->$hudsonTaskBatchFile->command;
    }
    
    public static function getStandardJobParamters()
    {
        $file = drupal_get_path('module', 'ateamutils').'/lib/config.xml';
        return simplexml_load_file($file);
    }
    
    /**
     * Changes to to config.xml preps
     * 
     * @param type $formValues
     */
    public static function setChanges($formValues)
    {
        $listOfAllCustomers = reportsuite::customerAlias(null, true);
        $data = array();
        
        //pre process form values into arbitary parameters for later usage
        foreach($formValues as $k => $v)
        {
            //drupal_set_message(substr($k, 0, 14));
            if(substr($k, 0, 14) == 'jobParameters:')
            {
                $split = explode(':', $k);
                $data['jobParams'][] = $split[1];
            }
            
            if($k == 'antCmd')
            {
                $data['antCmd'] = $v;
            }
            
        }
        
        if($formValues['selectJob'] === 'ALL')
        {
            foreach ($listOfAllCustomers as $customer)
            {
                if(!empty($customer['jenkinsJob']) && !isset($customer['disabled']))
                {
                    $customerName = str_replace(' ', '', $customer['name']);
                    self::putIntoXml($customerName, $customer['jenkinsJob'], $data , $creds);
                }
            }
        }
        else
        {
            $split = explode(':-:', $formValues['selectJob']);
            $customer = $split[0];
            $jenkinsJob = $split[1];
            
            self::putIntoXml(str_replace(' ', '', $customer), $jenkinsJob, $data , $creds);
        }
    }
    
    protected static function putIntoXml($customer, $jenkinsJob, $data ,$creds)
    {
        $libpath = drupal_get_path('module', 'ateamutils').'/lib';
        $putFileToPath = $libpath.'/configOnReqToProcess.xml';
        $cmd = "wget --auth-no-challenge --user=".$creds['u']." --password=".$creds['p']." -qO ".$putFileToPath." - ".$jenkinsJob."config.xml 2>&1";
        exec($cmd, $shellOut);
        
        $configXmlObj = simplexml_load_string(file_get_contents($putFileToPath));
        
        $properties = 'properties';

        $paramDefProperty = 'hudson.model.ParametersDefinitionProperty';

        $paramDef = 'parameterDefinitions';

        $stringParamDef = 'hudson.model.StringParameterDefinition';
    
        $confStringParams = array();
        foreach($configXmlObj->$properties->$paramDefProperty->$paramDef->$stringParamDef as $stringParams)
        {
            if(in_array((String)$stringParams->name, $data['jobParams']))
            {
                $k = array_keys($data['jobParams'], (String)$stringParams->name);
                unset($data['jobParams'][$k[0]]);
            }
        }
        
        foreach($data['jobParams'] as $jobParams)
        {
            $paramString = $configXmlObj->$properties->$paramDefProperty->
                            $paramDef->addChild($stringParamDef);
            $paramString->addChild('name', $jobParams);
            $paramString->addChild('description');
            $paramString->addChild('defaultValue');
        }
        
        $antCmd = str_replace('%customer%', $customer, $data['antCmd']);
        self::setBuildersForConfig($configXmlObj, $antCmd);
        $modifiedConfigFile = $libpath.'/'.$customer.'Config.xml';
        $configXmlObj->asXML($modifiedConfigFile);
        
        
        //temp to program commit to jenkins Job
        $cmdPost = "wget --auth-no-challenge --user=".$creds['u']." --password=".$creds['p']." --post-file=".$_SERVER['DOCUMENT_ROOT']."/drupal/".$modifiedConfigFile." ".$jenkinsJob."config.xml 2>&1";
        drupal_set_message($cmdPost);

        exec($cmdPost, $shellOut);
        drupal_set_message('<pre>' . print_r($shellOut, 1) . '</pre>');
    }
    
}

?>
