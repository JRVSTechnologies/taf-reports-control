<?php

function ateamutils_checkReleaseServerDiskSpace(){
    $output = "";
    $emailList = 'jossie.saul@datalex.com, Paul.Lester@Datalex.com, Adam.Worsley@Datalex.com, Gerard.Jacolbia@Datalex.com, Andrew.Saville@datalex.com';
    
    $diskSpaceData = file_get_contents('http://10.153.25.111/releases/diskSpaceStatus.txt');
    
    drupal_set_message('<pre>' . print_r($diskSpaceData, 1) . '</pre>');
    
    $arrExplode = explode(" ", $diskSpaceData);
    $pctg = null;
    foreach(array_filter($arrExplode) as $k=>$v){
        
        if(preg_match("/[0-9]+%/", $v)){
            drupal_set_message($v);
            $pctg = str_replace("%", "", $v);
            drupal_set_message($pctg);
        }
    }
    
    if(!empty($pctg))
    {
        if(intval($pctg)>95){
            drupal_set_message("LOW DISKSPACE");
            
            ateamutils_postmanSend('default_from', $emailList, '10.153.25.111 Release Server - LOW DISKSPACE', $diskSpaceData);
        }
    }
    
    $output .= $diskSpaceData;
    
    return $output;
}

function ateamutils_postmanSend($from = 'default_from', $to, $subject, $bodyContent)
{
        $module = 'ateamutils';
        $key = microtime();
        if ($from == 'default_from') {
          // Change this to your own default 'from' email address.
          $from = 'PSTSupportNotification@pstautomation.datalex.com';
        }

        $message = drupal_mail($module, $key, 
                   $to,language_default(), array(), $from, false);
        $message['headers']['Content-Type'] = 'text/html; charset=UTF-8; format=flowed';
        $message['body'] = $bodyContent;
        $message['subject'] = $subject;
        
        $system = drupal_mail_system($module, $key);
        //$message = $system->format($message);
        if ($system->mail($message)) {
            drupal_set_message("Email Sent");
          return TRUE;
        }
        else {
            drupal_set_message('<pre>' . print_r($res, 1) . '</pre>');
          return FALSE;
        }
}