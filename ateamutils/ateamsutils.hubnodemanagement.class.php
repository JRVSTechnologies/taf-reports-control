<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ateamsutils
 *
 * @author jossie.saul
 */
class ateamsutilshubnodemanagement {
    
    
    //put your code here
    
    public static function parseStatus($status){
        $output = '';
        if($status == 0){
        $output = theme('image', array(
                    'path' => drupal_get_path('module', 'ateamutils').
                              '/lib/images/ready.PNG',
                    'alt' => 'ready',
                    'title' => 'ReadyState',
                    'width' => '40%',
                    'height' => '75%',
                    ));
        }elseif ($status == 1){
        $output = theme('image', array(
                    'path' => drupal_get_path('module', 'ateamutils').
                              '/lib/images/inused.PNG',
                    'alt' => 'inused',
                    'title' => 'Inused',
                    'width' => '40%',
                    'height' => '75%',
                    ));            
        }
        return $output;
    }
    
    public static function overrideStatus($hpaid){
        
    }
    
    public static function determineOperationLink($status,$port){
     $output = '';
        if($status == 1){
            $output = l('Unlock', 
                    'ateamutils/hubPortProbe/overrideStatus/unlockPort/'.$port);
        }else if($status == 0){
            $output = l('Lock', 
                 'ateamutils/hubPortProbe/overrideStatus/lockPort/'.$port);
        }
      return $output;
    }
}
