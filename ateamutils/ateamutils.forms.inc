<?php

function ateamutils_jenkinsConfig_form($form, &$form_state)
{
 
    //drupal_set_message('Feature in Development. On Hold. 26th July 2013', 'warning');
    
    $listOfAllCustomers = reportsuite::customerAlias(null, true);
    $jobOptions = array('ALL' => 'All Jobs');
    
    foreach ($listOfAllCustomers as $customer)
    {
        if(!empty($customer['jenkinsJob']) && !isset($customer['disabled']))
        $jobOptions[$customer['name'].':-:'.$customer['jenkinsJob']] = $customer['name'];
    }
    
    if(!isset($form_state['values']['selectJob']) || 
            $form_state['values']['selectJob'] == 'ALL')
    {
        $specifiedSel = false;
        $title = 'Standard Configuration for All';
        $editableConfigs = ateamutilsjenkins::getfilteredConfigurableParams(
                ateamutilsjenkins::getStandardJobParamters());
    }
    else
    {
        $specifiedSel = true;
        $split = explode(':-:', $form_state['values']['selectJob']);
        $title = $split[0].' Jenkins Job Configuration';
        $editableConfigs = ateamutilsjenkins::getfilteredConfigurableParams(
                ateamutilsjenkins::getJobConfigXml($split[1], 2, 3));
    }
    
    $antCmd = $editableConfigs['builders']['batchFiles']['cmd'];
    
    /******************
     *  START OF FORM *
     ******************/
    $form['selectJob'] = array(
     '#type' => 'select',
     '#title' => t('Select Automation Jenkins Job'),
     '#options' => $jobOptions,
     '#ajax' => array(
         'callback' => 'ateamutils_jenkinsConfig_form_callback',
         'wrapper' => 'job-fieldset-wrapper',
     ),
    );
    
    $form['autoJenkinsJob'] = array(
       '#type' => 'fieldset',
       '#title' => $title,
       '#collapsible' => TRUE,
       '#collapsed' => FALSE,
       '#prefix' => '<div id="job-fieldset-wrapper">',
       '#suffix' => '</div>' 
    );
    
    
    $form['autoJenkinsJob']['jobParamTitle'] = array(
        '#markup' => '<h3><u>Active Parameters</u></h3>'
        );
    
    foreach($editableConfigs['jobParameters']['string'] as $config)
    {
        //$options[(string)$config->name] = (string)$config->name;
        $form['autoJenkinsJob']['jobParameters:'.(string)$config->name] = array(
          '#type' => 'checkbox',
          '#title' => (string)$config->name,
          '#default_value' => 1,
          '#disabled' => true,
        );
    }
    
    $form_state['additParameters'] = 
        !isset($form_state['additParameters'])?false:$form_state['additParameters'];
    
    $form_state['newParams'] = !isset($form_state['newParams'])?array():$form_state['newParams'];
    
    if(!empty($form_state['newParams']))
    {
        foreach($form_state['newParams'] as $params)
        {
            $form['autoJenkinsJob']['jobParameters:'.$params] = array(
            '#type' => 'checkbox',
            '#title' => $params,
            '#default_value' => 1,
            '#disabled' => true,
            );
            
            $antCmd .= ' -D'.$params.'=%'.$params.'%';
        }   
    }
    
    if($form_state['additParameters'])
    {
        
       $form['autoJenkinsJob']['additParam'] = array(
          '#type' => 'textfield',
          '#title' => 'Property ID',
          '#description' => t('This has to be unique'),
        );
       $paramSubmitTitleOp = 'Submit New Parameter';
    }
    else
    {
       $paramSubmitTitleOp = 'Add Paramater';
    }
    
    
    $form['autoJenkinsJob']['addParam'] = array(
        '#type' => 'submit',
        '#value' => t($paramSubmitTitleOp),
        '#ajax' => array(
          'callback' => 'ateamutils_jenkinsConfig_form_callback',
          'wrapper' => 'job-fieldset-wrapper',
        ),
    );

     if($form_state['additParameters'])
     {
         $form['autoJenkinsJob']['removeParam'] = array(
            '#type' => 'submit',
            '#value' => t('Cancel Add'),
            '#ajax' => array(
              'callback' => 'ateamutils_jenkinsConfig_form_callback',
              'wrapper' => 'job-fieldset-wrapper',
            ),
          );
     }
     
     $form['autoJenkinsJob']['antCmd'] = array(
         '#type' => 'textarea',
         '#title' => 'Ant Command',
         '#default_value' => $antCmd,
         '#disabled' => true,
         '#rows' => 10,
         '#resizable' => false,
     );
     
     $form['autoJenkinsJob']['change'] = array(
        '#type' => 'submit',
        '#value' => t('Commit Changes'),
        '#ajax' => array(
          'callback' => 'ateamutils_jenkinsConfig_form_callback',
          'wrapper' => 'job-fieldset-wrapper',
        ),
     );
     
     $form['autoJenkinsJob']['cancel'] = array(
        '#type' => 'submit',
        '#value' => t('Cancel'),
     );
     
    return $form;
}

function ateamutils_jenkinsConfig_form_callback($form, &$form_state)
{
    return $form['autoJenkinsJob'];
}

function ateamutils_jenkinsConfig_form_validate($form, &$form_state)
{
    
}

function ateamutils_jenkinsConfig_form_submit($form, &$form_state)
{
   switch($form_state['triggering_element']['#value'])
   {
       case 'Commit Changes':
           ateamutilsjenkins::setChanges($form_state['values']);
           break;
       case 'Add Paramater':
           $form_state['additParameters'] = true;$form_state['rebuild'] = TRUE;
           break;
       case 'Cancel Add':
           $form_state['additParameters'] = false;$form_state['rebuild'] = TRUE;
           break;
       case 'Submit New Parameter':
           drupal_set_message('Its Done');
           array_push($form_state['newParams'], $form_state['values']['additParam']);
           $form_state['additParameters'] = false;$form_state['rebuild'] = TRUE;
           break;
       case 'Cancel':
           drupal_goto('ateamutils');
           break;
   }
    
}